const {REG, static_field} = require('./constants'),
    {Utils} = require('./utils'),
    {Reg} = require('./custom-regex/Reg'),
    {ParseError} = require('./custom-error/parse-error'),
    {AddEscape} = require('./escape/add'),
    {RemoveEscape} = require('./escape/remove');

let indexOf = [].indexOf;
/**
 LineAnalyzer YML parsing
*/
class LineAnalyzer {
    /**
     * @param exceptionOnInvalidType
     * @param objectDecoder
     */
    static configure(exceptionOnInvalidType = null, objectDecoder = null) {
        // Update settings
        static_field.settings.exceptionOnInvalidType = exceptionOnInvalidType;
        static_field.settings.objectDecoder = objectDecoder;
    }

    /**
     Converts a YML string to a JavaScript object.
     @return [Object]  A JavaScript object representing the YML string
     @throw [ParseError]
     * @param value
     * @param exceptionOnInvalidType
     * @param objectDecoder
     */
    static parse(value, exceptionOnInvalidType = false, objectDecoder = null) {
        let context, result;
        // Update settings from last call of LineAnalyzer.parse()
        static_field.settings.exceptionOnInvalidType = exceptionOnInvalidType;
        static_field.settings.objectDecoder = objectDecoder;
        if (value == null) {
            return '';
        }
        value = Utils.trim(value);
        if (0 === value.length) {
            return '';
        }
        // Keep a context object to pass through static methods
        context = {
            exceptionOnInvalidType,
            objectDecoder,
            i: 0
        };
        switch (value.charAt(0)) {
            case '[':
                result = this.parseSequence(value, context);
                ++context.i;
                break;
            case '{':
                result = this.parseMapping(value, context);
                ++context.i;
                break;
            default:
                result = this.parseScalar(value, null, ['"', "'"], context);
        }
        // Some comments are allowed at the end
        if (REG.REG_TRAILING_COMMENTS.replace(value.slice(context.i), '') !== '') {
            throw new ParseError('Unexpected characters near "' + value.slice(context.i) + '".');
        }
        return result;
    }
    /**
     Parses a scalar to a YML string.
     @return [String]  A YML string
     @throw [ParseError] When malformed inline YML string is parsed
     * @param scalar
     * @param delimiters
     * @param stringDelimiters
     * @param context
     * @param evaluate
     */
    static parseScalar(scalar, delimiters = null, stringDelimiters = ['"', "'"], context = null, evaluate = true) {
        let i, joinedDelimiters, match, output, pattern, ref, ref1, strpos, tmp;
        if (context == null) {
            context = {
                exceptionOnInvalidType: static_field.settings.exceptionOnInvalidType,
                objectDecoder: static_field.settings.objectDecoder,
                i: 0
            };
        }
        ({i} = context);
        if (ref = scalar.charAt(i), indexOf.call(stringDelimiters, ref) >= 0) {
            // Quoted scalar
            output = this.parseQuotedScalar(scalar, context);
            ({i} = context);
            if (delimiters != null) {
                tmp = Utils.ltrim(scalar.slice(i), ' ');
                if (!(ref1 = tmp.charAt(0), indexOf.call(delimiters, ref1) >= 0)) {
                    throw new ParseError('Unexpected characters (' + scalar.slice(i) + ').');
                }
            }
        } else {
            // "normal" string
            if (!delimiters) {
                output = scalar.slice(i);
                i += output.length;
                // Remove comments
                strpos = output.indexOf(' #');
                if (strpos !== -1) {
                    output = Utils.rtrim(output.slice(0, strpos));
                }
            } else {
                joinedDelimiters = delimiters.join('|');
                pattern = static_field.REG_SCALAR_BY_DELIMITERS[joinedDelimiters];
                if (pattern == null) {
                    pattern = new Reg('^(.+?)(' + joinedDelimiters + ')');
                    static_field.REG_SCALAR_BY_DELIMITERS[joinedDelimiters] = pattern;
                }
                if (match = pattern.exec(scalar.slice(i))) {
                    output = match[1];
                    i += output.length;
                } else {
                    throw new ParseError('Malformed inline YML string (' + scalar + ').');
                }
            }
            if (evaluate) {
                output = this.evaluateScalar(output, context);
            }
        }
        context.i = i;
        return output;
    }

    /**
     Parses a quoted scalar to YML.
     @return [String]  A YML string
     @throw [ParseError] When malformed inline YML string is parsed
     * @param scalar
     * @param context
     */
    static parseQuotedScalar(scalar, context) {
        let i, match, output;
        ({i} = context);
        if (!(match = REG.REG_QUOTED_SCALAR.exec(scalar.slice(i)))) {
            throw new ParseError('Malformed inline YML string (' + scalar.slice(i) + ').');
        }
        output = match[0].substr(1, match[0].length - 2);
        if ('"' === scalar.charAt(i)) {
            output = AddEscape.unescapeDoubleQuotedString(output);
        } else {
            output = AddEscape.unescapeSingleQuotedString(output);
        }
        i += match[0].length;
        context.i = i;
        return output;
    }

    /**
     Parses a sequence to a YML string.
     @return [String]  A YML string
     @throw [ParseError] When malformed inline YML string is parsed
     * @param sequence
     * @param context
     */

    static parseSequence(sequence, context) {
        let e, i, isQuoted, len, output, ref, value;
        output = [];
        len = sequence.length;
        ({i} = context);
        i += 1;
        // [foo, bar, ...]
        while (i < len) {
            context.i = i;
            switch (sequence.charAt(i)) {
                case '[':
                    // Nested sequence
                    output.push(this.parseSequence(sequence, context));
                    ({i} = context);
                    break;
                case '{':
                    // Nested mapping
                    output.push(this.parseMapping(sequence, context));
                    ({i} = context);
                    break;
                case ']':
                    return output;
                case ',':
                case ' ':
                case '\n':
                    break;
                default:
                    // Do nothing
                    isQuoted = ((ref = sequence.charAt(i)) === '"' || ref === "'");
                    value = this.parseScalar(sequence, [',', ']'], ['"', "'"], context);
                    ({i} = context);
                    if (!isQuoted && typeof value === 'string' && (value.indexOf(': ') !== -1 || value.indexOf(':\n') !== -1)) {
                        try {
                            // Embedded mapping?
                            value = this.parseMapping('{' + value + '}');
                        } catch (error) {
                            e = error;
                        }
                    }
                    // No, it's not
                    output.push(value);
                    --i;
            }
            ++i;
        }
        throw new ParseError('Malformed inline YML string ' + sequence);
    }

    /**
     Parses a mapping to a YML string.
     @return [String]  A YML string
     @throw [ParseError] When malformed inline YML string is parsed
     * @param mapping
     * @param context
     */
    static parseMapping(mapping, context) {
        let done, i, key, len, output, shouldContinueWhileLoop, value;
        output = {};
        len = mapping.length;
        ({i} = context);
        i += 1;
        // {foo: bar, bar:foo, ...}
        shouldContinueWhileLoop = false;
        while (i < len) {
            context.i = i;
            switch (mapping.charAt(i)) {
                case ' ':
                case ',':
                case '\n':
                    ++i;
                    context.i = i;
                    shouldContinueWhileLoop = true;
                    break;
                case '}':
                    return output;
            }
            if (shouldContinueWhileLoop) {
                shouldContinueWhileLoop = false;
                continue;
            }
            // Key
            key = this.parseScalar(mapping, [':', ' ', '\n'], ['"', "'"], context, false);
            ({i} = context);
            // Value
            done = false;
            while (i < len) {
                context.i = i;
                switch (mapping.charAt(i)) {
                    case '[':
                        // Nested sequence
                        value = this.parseSequence(mapping, context);
                        ({i} = context);
                        // Spec: Keys MUST be unique; first one wins.
                        // Parser cannot abort this mapping earlier, since lines
                        // are processed sequentially.
                        if (output[key] === void 0) {
                            output[key] = value;
                        }
                        done = true;
                        break;
                    case '{':
                        // Nested mapping
                        value = this.parseMapping(mapping, context);
                        ({i} = context);
                        // Spec: Keys MUST be unique; first one wins.
                        // Parser cannot abort this mapping earlier, since lines
                        // are processed sequentially.
                        if (output[key] === void 0) {
                            output[key] = value;
                        }
                        done = true;
                        break;
                    case ':':
                    case ' ':
                    case '\n':
                        break;
                    default:
                        // Do nothing
                        value = this.parseScalar(mapping, [',', '}'], ['"', "'"], context);
                        ({i} = context);
                        // Spec: Keys MUST be unique; first one wins.
                        // Parser cannot abort this mapping earlier, since lines
                        // are processed sequentially.
                        if (output[key] === void 0) {
                            output[key] = value;
                        }
                        done = true;
                        --i;
                }
                ++i;
                if (done) {
                    break;
                }
            }
        }
        throw new ParseError('Malformed inline YML string ' + mapping);
    }

    /**
     Evaluates scalars and replaces magic values.
     @return [String]  A YML string
     * @param scalar
     * @param context
     */
    static evaluateScalar(scalar, context) {
        let cast, date, exceptionOnInvalidType, firstChar, firstSpace, firstWord, objectDecoder, raw, scalarLower,
            subValue, trimmedScalar;
        scalar = Utils.trim(scalar);
        scalarLower = scalar.toLowerCase();
        switch (scalarLower) {
            case 'null':
            case '':
            case '~':
                return null;
            case 'true':
                return true;
            case 'false':
                return false;
            case '.inf':
                return 2e308;
            case '.nan':
                return 0 / 0;
            case '-.inf':
                return 2e308;
            default:
                firstChar = scalarLower.charAt(0);
                switch (firstChar) {
                    case '!':
                        firstSpace = scalar.indexOf(' ');
                        if (firstSpace === -1) {
                            firstWord = scalarLower;
                        } else {
                            firstWord = scalarLower.slice(0, firstSpace);
                        }
                        switch (firstWord) {
                            case '!':
                                if (firstSpace !== -1) {
                                    return parseInt(this.parseScalar(scalar.slice(2)));
                                }
                                return null;
                            case '!str':
                                return Utils.ltrim(scalar.slice(4));
                            case '!!str':
                                return Utils.ltrim(scalar.slice(5));
                            case '!!int':
                                return parseInt(this.parseScalar(scalar.slice(5)));
                            case '!!bool':
                                return Utils.parseBoolean(this.parseScalar(scalar.slice(6)), false);
                            case '!!float':
                                return parseFloat(this.parseScalar(scalar.slice(7)));
                            case '!!timestamp':
                                return Utils.stringToDate(Utils.ltrim(scalar.slice(11)));
                            default:
                                if (context == null) {
                                    context = {
                                        exceptionOnInvalidType: static_field.settings.exceptionOnInvalidType,
                                        objectDecoder: static_field.settings.objectDecoder,
                                        i: 0
                                    };
                                }
                                ({objectDecoder, exceptionOnInvalidType} = context);
                                if (objectDecoder) {
                                    // If objectDecoder function is given, we can do custom decoding of custom types
                                    trimmedScalar = Utils.rtrim(scalar);
                                    firstSpace = trimmedScalar.indexOf(' ');
                                    if (firstSpace === -1) {
                                        return objectDecoder(trimmedScalar, null);
                                    } else {
                                        subValue = Utils.ltrim(trimmedScalar.slice(firstSpace + 1));
                                        if (!(subValue.length > 0)) {
                                            subValue = null;
                                        }
                                        return objectDecoder(trimmedScalar.slice(0, firstSpace), subValue);
                                    }
                                }
                                if (exceptionOnInvalidType) {
                                    throw new ParseError('Custom object support when parsing a YML file has been disabled.');
                                }
                                return null;
                        }
                        break;
                    case '0':
                        if ('0x' === scalar.slice(0, 2)) {
                            return Utils.hexDec(scalar);
                        } else if (Utils.isDigits(scalar)) {
                            return Utils.octDec(scalar);
                        } else if (Utils.isNumeric(scalar)) {
                            return parseFloat(scalar);
                        } else {
                            return scalar;
                        }
                        break;
                    case '+':
                        if (Utils.isDigits(scalar)) {
                            raw = scalar;
                            cast = parseInt(raw);
                            if (raw === String(cast)) {
                                return cast;
                            } else {
                                return raw;
                            }
                        } else if (Utils.isNumeric(scalar)) {
                            return parseFloat(scalar);
                        } else if (REG.REG_THOUSAND_NUMERIC_SCALAR.test(scalar)) {
                            return parseFloat(scalar.replace(',', ''));
                        }
                        return scalar;
                    case '-':
                        if (Utils.isDigits(scalar.slice(1))) {
                            if ('0' === scalar.charAt(1)) {
                                return -Utils.octDec(scalar.slice(1));
                            } else {
                                raw = scalar.slice(1);
                                cast = parseInt(raw);
                                if (raw === String(cast)) {
                                    return -cast;
                                } else {
                                    return -raw;
                                }
                            }
                        } else if (Utils.isNumeric(scalar)) {
                            return parseFloat(scalar);
                        } else if (REG.REG_THOUSAND_NUMERIC_SCALAR.test(scalar)) {
                            return parseFloat(scalar.replace(',', ''));
                        }
                        return scalar;
                    default:
                        if (date = Utils.stringToDate(scalar)) {
                            return date;
                        } else if (Utils.isNumeric(scalar)) {
                            return parseFloat(scalar);
                        } else if (REG.REG_THOUSAND_NUMERIC_SCALAR.test(scalar)) {
                            return parseFloat(scalar.replace(',', ''));
                        }
                        return scalar;
                }
        }
    }

}

module.exports = {LineAnalyzer};
