const fs = require('fs');
const path = require('path');
const {REG, reg, static_field, fromCharCode} = require('./constants');
const {Reg} = require('./custom-regex/Reg');

const hasProp = {}.hasOwnProperty;
/**
    Class A bunch of utility methods
*/

class Utils {
    /**
     Precompiled date pattern
     Trims the given string on both sides
     @return [String] A trimmed string
     * @param str
     * @param _char
     */
    static trim(str, _char = '\\s') {
        let regexLeft, regexRight;
        regexLeft = static_field.REGEX_LEFT_TRIM_BY_CHAR[_char];
        if (regexLeft == null) {
            static_field.REGEX_LEFT_TRIM_BY_CHAR[_char] = regexLeft = new RegExp('^' + _char + '' + _char + '*');
        }
        regexLeft.lastIndex = 0;
        regexRight = static_field.REGEX_RIGHT_TRIM_BY_CHAR[_char];
        if (regexRight == null) {
            static_field.REGEX_RIGHT_TRIM_BY_CHAR[_char] = regexRight = new RegExp(_char + '' + _char + '*$');
        }
        regexRight.lastIndex = 0;
        return str.replace(regexLeft, '').replace(regexRight, '');
    }

    /**
     Trims the given string on the left side
     @return [String] A trimmed string
     * @param str
     * @param _char
     */
    static ltrim(str, _char = '\\s') {
        let regexLeft;
        regexLeft = static_field.REGEX_LEFT_TRIM_BY_CHAR[_char];
        if (regexLeft == null) {
            static_field.REGEX_LEFT_TRIM_BY_CHAR[_char] = regexLeft = new RegExp('^' + _char + '' + _char + '*');
        }
        regexLeft.lastIndex = 0;
        return str.replace(regexLeft, '');
    }

    /**
     Checks if the given value is empty (null, undefined, empty string, string '0', empty Array, empty Object)
     @return [Boolean] true if the value is empty
     * @param value
     */
    static isEmpty(value) {
        return !value || value === '' || value === '0' || (value instanceof Array && value.length === 0) || this.isEmptyObject(value);
    }

    /**
     Reads the data from the given file path and returns the result as string
     @return [String]  The resulting data as string
     * @param path
     * @param callback
     */
    static getStringFromFile(path, callback = null) {
        let data;
        if (callback != null) {
            // Async
            return fs.readFile(path, function(err, data) {
                if (err) {
                    return callback(null);
                } else {
                    return callback(String(data));
                }
            });
        } else {
            // Sync
            data = fs.readFileSync(path);
            if (data != null) {
                return String(data);
            }
            return null;
        }
    }
    /**
     listing all files in directoryPath
     @return [Array] Array of files
     * @param directoryPath
     */
    static getFiles (directoryPath) {
        return fs.readdirSync(directoryPath).map(name => ({
            name,
            path: `${directoryPath}/${name}`,
            absolutePath: path.resolve(__dirname, `${directoryPath}/${name}`)
        }));
    }

    /**
     Returns a parsed date from the given string
     @return [Date] The parsed date or null if parsing failed
     * @param str
     */
    static stringToDate(str) {
        let date, day, fraction, hour, info, minute, month, second, tz_hour, tz_minute, tz_offset, year;
        if (!(str != null ? str.length : void 0)) {
            return null;
        }
        // Perform regular expression pattern
        info = REG.REG_DATE.exec(str);
        if (!info) {
            return null;
        }
        // Extract year, month, day
        year = parseInt(info.year, 10);
        month = parseInt(info.month, 10) - 1; // In javascript, january is 0, february 1, etc...
        day = parseInt(info.day, 10);
        // If no hour is given, return a date with day precision
        if (info.hour == null) {
            date = new Date(Date.UTC(year, month, day));
            return date;
        }
        // Extract hour, minute, second
        hour = parseInt(info.hour, 10);
        minute = parseInt(info.minute, 10);
        second = parseInt(info.second, 10);
        // Extract fraction, if given
        if (info.fraction != null) {
            fraction = info.fraction.slice(0, 3);
            while (fraction.length < 3) {
                fraction += '0';
            }
            fraction = parseInt(fraction, 10);
        } else {
            fraction = 0;
        }
        // Compute timezone offset if given
        if (info.tz != null) {
            tz_hour = parseInt(info.tz_hour, 10);
            if (info.tz_minute != null) {
                tz_minute = parseInt(info.tz_minute, 10);
            } else {
                tz_minute = 0;
            }
            // Compute timezone delta in ms
            tz_offset = (tz_hour * 60 + tz_minute) * 60000;
            if ('-' === info.tz_sign) {
                tz_offset *= -1;
            }
        }
        // Compute date
        date = new Date(Date.UTC(year, month, day, hour, minute, second, fraction));
        if (tz_offset) {
            date.setTime(date.getTime() - tz_offset);
        }
        return date;
    }

    /**
     Returns true if input is numeric
     @return [Boolean] true if input is numeric
     * @param input
     */
    static isNumeric(input) {
        reg.REGEX_SPACES.lastIndex = 0;
        return typeof input === 'number' || typeof input === 'string' && !isNaN(input) && input.replace(reg.REGEX_SPACES, '') !== '';
    }

    /**
     Checks if the given value is an empty object
     @return [Boolean] true if the value is empty and is an object
     * @param value
     */
    static isEmptyObject(value) {
        let k;
        return value instanceof Object && ((function() {
            let results;
            results = [];
            for (k in value) {
                if (!hasProp.call(value, k)) {
continue;
}
                results.push(k);
            }
            return results;
        })()).length === 0;
    }

    /**
     Returns true if input is only composed of digits
     @return [Boolean] true if input is only composed of digits
     * @param input
     */
    static isDigits(input) {
        reg.REGEX_DIGITS.lastIndex = 0;
        return reg.REGEX_DIGITS.test(input);
    }

    /**
     Decode octal value
     @return [Integer] The decoded value
     * @param input
     */
    static octDec(input) {
        reg.REGEX_OCTAL.lastIndex = 0;
        return parseInt((input + '').replace(reg.REGEX_OCTAL, ''), 8);
    }

    /**
     Counts the number of occurences of subString inside string
     @return [Integer] The number of occurences
     * @param string
     * @param subString
     * @param start
     * @param length
     */
    static subStrCount(string, subString, start, length) {
        let c, i, j, len, ref, sublen;
        c = 0;
        string = '' + string;
        subString = '' + subString;
        if (start != null) {
            string = string.slice(start);
        }
        if (length != null) {
            string = string.slice(0, length);
        }
        len = string.length;
        sublen = subString.length;
        for (i = j = 0, ref = len; (0 <= ref ? j < ref : j > ref); i = 0 <= ref ? ++j : --j) {
            if (subString === string.slice(i, sublen)) {
                c++;
                i += sublen - 1;
            }
        }
        return c;
    }

    /**
     Decode hexadecimal value
     @return [Integer] The decoded value
     * @param input
     */
    static hexDec(input) {
        this.REGEX_HEXADECIMAL.lastIndex = 0;
        input = this.trim(input);
        if ((input + '').slice(0, 2) === '0x') {
            input = (input + '').slice(2);
        }
        return parseInt((input + '').replace(this.REGEX_HEXADECIMAL, ''), 16);
    }

    /**
     Get the UTF-8 character for the given code point.
     @return [String] The corresponding UTF-8 character
     * @param c
     */
    static utf8chr(c) {
        if (0x80 > (c %= 0x200000)) {
            return fromCharCode(c);
        }
        if (0x800 > c) {
            return fromCharCode(0xC0 | c >> 6) + fromCharCode(0x80 | c & 0x3F);
        }
        if (0x10000 > c) {
            return fromCharCode(0xE0 | c >> 12) + fromCharCode(0x80 | c >> 6 & 0x3F) + fromCharCode(0x80 | c & 0x3F);
        }
        return fromCharCode(0xF0 | c >> 18) + fromCharCode(0x80 | c >> 12 & 0x3F) + fromCharCode(0x80 | c >> 6 & 0x3F) + fromCharCode(0x80 | c & 0x3F);
    }
}

module.exports = {Utils};
